// [rajeshs@gw02 ~]$ hdfs dfs -mkdir /user/rajeshs/questions/

// [cloudera@quickstart retail_db]$ pwd
// /home/cloudera/data/retail_db

/*
1) sqoop import with only few columns
Sqoop import from account table only with selected columns
*/
/*
2) sqoop export with tab delimited data ..export to mysql provided table
Sqoop export 25 million records. data was tab delimited in hdfs
*/

/*
3) select data where charge > 10$. Save as parquet gzip format
Hive metastore table is given. Problem3 is the database and billing is the table
name. Get the records from billing table where charges > 10. billing table in hive
metastore need to read and charge>10 and save output as parquet file gzip compression.
*/
